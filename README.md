# ❄️ Приют стужи
Постоялый двор «Приют стужи» - это маленький уютный уголок, где можно укрыться от бесконечного холода, окружающего его со всех сторон. Герои ищут ответы на загадочное убийство, которое связано с этим местом. Однако здесь никому нельзя доверять, ведь зло может принять облик любого постояльца.

Посмотреть видео с обзором приключения можно [тут](https://youtu.be/J1wG2QeDl0c)

## Обратная связь
Обсудить модуль и предложить улучшения можно [VK](https://vk.com/adventure_guys), [Telegram](https://t.me/adventureguysrpg) или на [Discord сервере](https://discord.gg/xefpN9789E)

## Поддержать авторов
Если вам понравился модуль, и вы хотите меня поддержать, вы можете сделать это на нашей [страничке Boosty](https://boosty.to/omar0275). Спасибо!

## Сбор аналитики. Отказ от ответственности
Этот модуль собирает анонимные данные: версию Foundry VTT, версию системы, версию модуля, страну, ошибки и конфликты. Если вы не принимаете эти условия, пожалуйста, прекратите использование модуля.

## Файловая структура
Если вы не используете Foundry VTT, то вы все равно можете использовать материалы из модуля для своих игр.
Все они находятся в папке assets.

- actors – папка с изображениями персонажей/чудовищ
  - portraits – полноразмерные изображения
  - tokens – круглые изображения токенов персонажей для использования на картах
- art – арты и раздаточные материалы из текста приключения
- audio – музыка и звуки
- scenes – карты сцен из приключения
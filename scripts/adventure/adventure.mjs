import * as handlers from './importer.mjs';

/**
 * Configuration of the adventure being exported/imported
 */
export const ADVENTURE = {
  // Basic information about the module and the adventure compendium it contains
  moduleName: 'ag-priyut-stuzhi',
  packName: 'ag-priyut-stuzhi-adventure',
  packId: 'ag-priyut-stuzhi.ag-priyut-stuzhi-adventure',
  adventureUuid:
    'Compendium.ag-priyut-stuzhi.ag-priyut-stuzhi-adventure.Adventure.eN84tFHYX61GSFg6',
  adventureId: 'eN84tFHYX61GSFg6',
  changelogId: 'HplIcXcGYUfoBXZ3',

  // A CSS class to automatically apply to application windows which belong to this module
  cssClass: 'ag-priyut-stuzhi',

  description: `Постоялый двор «Приют стужи» - это маленький уютный уголок, где можно укрыться от бесконечного холода, окружающего его со всех сторон. Герои ищут ответы на загадочное убийство, которое связано с этим местом. Однако здесь никому нельзя доверять, ведь зло может принять облик любого постояльца.`,

  // Define special Import Options with custom callback logic
  importOptions: {
    displayJournal: {
      label: 'Открыть текст приключения',
      default: true,
      handler: handlers.displayJournal,
      documentId: 'XCG3sw21YriGXwFc',
    },
    customizeJoin: {
      label: 'Преобразовать игровой мир',
      default: false,
      handler: handlers.customizeJoin,
      background: 'modules/ag-priyut-stuzhi/assets/art/adventure-cover.webp',
    },
  },
};
